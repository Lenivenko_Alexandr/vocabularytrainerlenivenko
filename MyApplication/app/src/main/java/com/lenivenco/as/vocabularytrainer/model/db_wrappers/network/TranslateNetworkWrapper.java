package com.lenivenco.as.vocabularytrainer.model.db_wrappers.network;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class TranslateNetworkWrapper {
    private static final String BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate ?";
    private static final String URL = "https://translate.yandex.net/api/v1.5/tr/getLangs ? " +
            "key=<trnsl.1.1.20161112T103342Z.1a6100af2b74a0b0.c21de8af74a027455ea43dd78004158fb4646c9b>" +
            " & ui=<ru>";
    private static final String KEY = "key=<trnsl.1.1.20161112T103342Z.1a6100af2b74a0b0.c21de8af74a027455ea43dd78004158fb4646c9b>";
    private static final String TEXT = " & text=";
    private static final String LANG = " & lang=en-ru";
    private static final String END = " & [format=<HTTP>]" +" & [options=<1.1>]";



    private Context m_Context = null;

    public TranslateNetworkWrapper(Context context) {
        this.m_Context = context;
    }

    public String getTranslation(String strText) throws IOException, JSONException {

        String urlStr = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20161112T103342Z.1a6100af2b74a0b0.c21de8af74a027455ea43dd78004158fb4646c9b";
        URL urlObj = new URL(urlStr);
        HttpsURLConnection connection = (HttpsURLConnection)urlObj.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
        dataOutputStream.writeBytes("text=" + URLEncoder.encode(strText, "UTF-8") + "&lang=" + "ru");

        InputStream response = connection.getInputStream();

        String strResponse = "";

        String strTransl = "";
        int nResponseCode = connection.getResponseCode();
        if (nResponseCode==200){
            InputStream is = connection.getInputStream();

            if (is!=null){
                BufferedReader reader = null;
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                if (reader!=null){
                    StringBuilder stringBuilder = new StringBuilder();
                    while (true){
                        String tempStr = reader.readLine();
                        if(tempStr != null){
                            stringBuilder.append(tempStr);
                        } else {
                            strResponse = stringBuilder.toString();
                            break;
                        }
                    }
                }
            }
            JSONObject jsonObject = new JSONObject(strResponse);
            JSONArray jsonArray = jsonObject.getJSONArray("text");
            strResponse = jsonArray.getString(0);
            //JSONArray arrData = new JSONArray(strResponse);

            /*for (int i=0; i<arrData.length(); i++){

                strTransl = arrData.getString(i);
            }*/
            connection.disconnect();
        }
        return strResponse;

    }

    public Context getContext() {
        return m_Context;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    private String createPost(String strTextForTranslation){
        return BASE_URL+KEY+TEXT+"<"+strTextForTranslation+">"+LANG + END;
    }
}
