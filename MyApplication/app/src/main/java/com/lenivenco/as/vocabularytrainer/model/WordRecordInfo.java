package com.lenivenco.as.vocabularytrainer.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;


public class WordRecordInfo extends Entity {

    private String m_strOriginalWord="";
    private String m_strDescription="";
    private long m_nUnitId = -1;
    private boolean m_bLearntWord=false;
    private int m_numberTrueAnswer = 0; ;


    public WordRecordInfo(String strOriginalWord, String strDescription,long nUnitId ){
        setId(-1);
        setOriginalWord(strOriginalWord);
        setDescription(strDescription);
        m_nUnitId = nUnitId;
        m_bLearntWord=false;
    }
    public WordRecordInfo(){

    }

    public WordRecordInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.WordsTable.TABLE_FIELD_ID)));
        setOriginalWord(cursor.getString(cursor.getColumnIndex(UnitsDBConstants.WordsTable.TABLE_FIELD_ORIGINAL_WORD)));
        setDescription(cursor.getString(cursor.getColumnIndex(UnitsDBConstants.WordsTable.TABLE_FIELD_DESCRIPTION_WORD)));
        setLearntWord(cursor.getInt(cursor.getColumnIndex(UnitsDBConstants.WordsTable.TABLE_FIELD_UNIT_IS_WORD_LEARNED)));
    }

    public WordRecordInfo(WordRecordInfo wordRecordInfo) {
        setId(wordRecordInfo.getId());
        setOriginalWord(wordRecordInfo.getOriginalWord());
        setDescription(wordRecordInfo.getDescription());
        m_nUnitId = wordRecordInfo.getUnitId();
        m_bLearntWord=wordRecordInfo.isLearntWord();
        m_numberTrueAnswer = wordRecordInfo.getNumberTrueAnswer();

    }

    @Override
    public ContentValues getContentValue() {
        int nFlag = 0;
        ContentValues values = new ContentValues();
        values.put(UnitsDBConstants.WordsTable.TABLE_FIELD_ORIGINAL_WORD,getOriginalWord());
        values.put(UnitsDBConstants.WordsTable.TABLE_FIELD_DESCRIPTION_WORD,getDescription());
        /*if(isLearntWord()){
            nFlag = 1;
        }*/
        values.put(UnitsDBConstants.WordsTable.TABLE_FIELD_UNIT_IS_WORD_LEARNED,getNumberTrueAnswer());

        return values;
    }

    public String getOriginalWord() {
        return m_strOriginalWord;
    }

    public void setOriginalWord(String m_strOriginalWord) {
        this.m_strOriginalWord = m_strOriginalWord.trim();
    }

    public String getDescription() {
        return m_strDescription;
    }

    public void setDescription(String m_strDescription) {
        this.m_strDescription = m_strDescription.trim();
    }


    public long getUnitId() {
        return m_nUnitId;
    }

    public void setUnitId(long m_nUnitId) {
        this.m_nUnitId = m_nUnitId;
    }

    public boolean isLearntWord() {
        return m_bLearntWord;
    }

    //number 3 it is quantity of necessary correct answer
    public void setLearntWord(int nLearntWord) {
        if(nLearntWord==3){
            m_bLearntWord=true;

        }else {
            m_bLearntWord = false;
        }
        m_numberTrueAnswer = nLearntWord;
    }

    public void setLearntWord(boolean bLearntWord) {
        if(bLearntWord){

            setLearntWord(3);
        }else {
            m_bLearntWord = false;
        }
    }


    public int getNumberTrueAnswer() {
        return m_numberTrueAnswer;
    }

    public void setNumberTrueAnswer(int number) {
        m_numberTrueAnswer = number;
    }


    public void increaseNumberTrueAnswer() {
        m_numberTrueAnswer++;
    }

    public void setNullNumberTrueAnswer() {
        m_numberTrueAnswer=0;
    }

}
