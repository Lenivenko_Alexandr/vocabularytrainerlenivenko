package com.lenivenco.as.vocabularytrainer.activities;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.adapters.ShowWordsListBaseAdapter;
import com.lenivenco.as.vocabularytrainer.dialogs.EditUnitDialog;
import com.lenivenco.as.vocabularytrainer.loaders.TranslateLoader;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.network.TranslateNetworkWrapper;
import com.lenivenco.as.vocabularytrainer.model.engines.UnitEngine;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class ComposeActivity extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<String> {

    private static final int TRANSLATE_LOADER_ID = 1;

    private final String YANDEX_REQUEST_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?";
    private final String KEY = "trnsl.1.1.20170206T202707Z.a33b20d6b1047f83.f69b27c5c63d1afb97a4105706278bec15fa1986";

    public static final String KEY_ID = "KEY_ID";
    private static final String UPDATE_TAG = "update_tag";
    private static final String  INSERT_TAG = "insert_tag";



    private long m_unitId =-1;
    private ArrayList<WordRecordInfo> m_arrWordsData = new ArrayList<>();

    private ListView m_wordsListListView = null;
    private ShowWordsListBaseAdapter m_wordsAdapter = null;
    private EditText m_originalWordEditText = null;
    private EditText m_descriptionWordEditText = null;
    private WordRecordInfo m_wordRecordInfo = null;
    private Button m_addAndUpdateButton = null;
    private TextView m_titleTextView=null;
    private UnitEngine m_unitEngine = null;
    private UnitRecordInfo m_unitRecordInfo = null;
    private WordEngine m_wordEngine = null;
    private Button m_cancelButton;
    private Menu m_menu = null;
    private TranslateLoader mTranslataLoader;
    private View mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        findView();
        drawToolBar();


        m_unitEngine = new UnitEngine(this);
        m_wordEngine = new WordEngine(this);
        Intent intent  = getIntent();

        if(!intent.equals(null)){
            Bundle bundle = intent.getExtras();
            if (!bundle.equals(null)){
                m_unitId = (long) bundle.getLong(KEY_ID,-1);

            }
        }

        if(m_unitId==-1){
            intent = new Intent(this, EditUnitDialog.class);
            intent.putExtra(KEY_ID,m_unitId);
            startActivityForResult(intent, EditUnitDialog.REQUEST_COD_EDIT_UNIT_DIALOG);

        }else{

            recreateList();
            m_unitRecordInfo = m_unitEngine.getItemById(m_unitId);
            m_titleTextView.setText(m_unitRecordInfo.getUnitName());
        }

    }

    private void drawToolBar() {
        Toolbar m_toolbar = (Toolbar) findViewById(R.id.toolbarComposeActivity);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle(getResources().getString(R.string.compose_activity_name));

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_delete);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setOverflowIcon(menuIconDrawable);

    }

    @Override
    protected void onResume() {
        super.onResume();
        recreateList();
    }

    private void recreateList() {
        if(m_unitId!=-1){
            m_arrWordsData.clear();
            m_arrWordsData.addAll(m_wordEngine.getAllByUnitId(m_unitId));
            m_wordsAdapter.notifyDataSetChanged();
        }

    }

    private void findView() {
        //buttons
        m_addAndUpdateButton = (Button) findViewById(R.id.addWordButtonComposeActivity);
        m_addAndUpdateButton.setOnClickListener(this);
        m_addAndUpdateButton.setTag(INSERT_TAG);
        m_cancelButton =(Button) findViewById(R.id.cancelButtonComposeActivity);
        m_cancelButton.setOnClickListener(this);

        //ListView and adapter
        m_wordsAdapter = new ShowWordsListBaseAdapter(m_arrWordsData);

        m_wordsListListView = (ListView) findViewById(R.id.fullListWordListViewComposeActivity);
        m_wordsListListView.setOnItemClickListener(this);
        m_wordsListListView.setAdapter(m_wordsAdapter);


        // editText
        m_originalWordEditText = (EditText) findViewById(R.id.originalWordEditTextComposeActivity);
        m_descriptionWordEditText = (EditText) findViewById(R.id.descriptionWordEditTextComposeActivity);

        // TextView
        m_titleTextView = (TextView) findViewById(R.id.unitTitleTextViewComposeActivity);
        m_titleTextView.setOnClickListener(this);

        //progress bar
        mProgressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View view) {

        m_unitEngine.getItemById(m_unitId).setUpdateDateTime();
        switch (view.getId()){
            case R.id.addWordButtonComposeActivity:

                //final String strOriginal = m_originalWordEditText.getText().toString();
                String strDescription = m_descriptionWordEditText.getText().toString();
                //final String[] strResult = new String[1];
                if(strDescription.isEmpty()){

                    if(!isOnline()){
                        Toast.makeText(this,"No intenet connectio",Toast.LENGTH_LONG).show();
                        return;
                    }

                    // Get a reference to the LoaderManager, in order to interact with loaders.
                    android.app.LoaderManager loaderManager = getLoaderManager();
                    if(mTranslataLoader!=null){

                        loaderManager.destroyLoader(TRANSLATE_LOADER_ID);
                    }

                    // Initialize the loader. Pass in the int ID constant defined above and pass in null for
                    // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
                    // because this activity implements the LoaderCallbacks interface).
                    mTranslataLoader = (TranslateLoader) loaderManager.initLoader(TRANSLATE_LOADER_ID, null, ComposeActivity.this);

                }


                break;
            case R.id.unitTitleTextViewComposeActivity:
                Intent intent = new Intent(this, EditUnitDialog.class);
                intent.putExtra(KEY_ID,m_unitId);
                startActivityForResult(intent, EditUnitDialog.REQUEST_COD_EDIT_UNIT_DIALOG);
                break;
            case R.id.cancelButtonComposeActivity:
                setInsetTegToAddButton();
                clearEditText();
                recreateList();
                m_cancelButton.setVisibility(View.INVISIBLE);
                break;
        }
        onPrepareOptionsMenu(m_menu);
    }

    private void setInsetTegToAddButton() {
        m_addAndUpdateButton.setTag(INSERT_TAG);
        m_addAndUpdateButton.setText(getResources().getString(R.string.add));

    }

    private void clearEditText() {
        m_originalWordEditText.setText("");
        m_originalWordEditText.requestFocus();
        m_descriptionWordEditText.setText("");
        m_cancelButton.setVisibility(View.INVISIBLE);
        //m_addAndUpdateButton.setText(getResources().getString(R.string.add));
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {



        m_wordRecordInfo = m_arrWordsData.get(position);
        m_originalWordEditText.setText(m_wordRecordInfo.getOriginalWord());
        m_descriptionWordEditText.setText(m_wordRecordInfo.getDescription());
        m_addAndUpdateButton.setText(getResources().getString(R.string.updateButton));
        m_addAndUpdateButton.setTag(UPDATE_TAG);
        m_addAndUpdateButton.setTag(R.id.addWordButtonComposeActivity,id);

        m_cancelButton.setVisibility(View.VISIBLE);
        onPrepareOptionsMenu(m_menu);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode==RESULT_OK) {
            if (requestCode == EditUnitDialog.REQUEST_COD_EDIT_UNIT_DIALOG) {
                if(m_unitId==-1){

                    if(intent!=null){
                        Bundle bundle = intent.getExtras();
                        if(bundle!=null){
                            m_unitId = bundle.getLong(ComposeActivity.KEY_ID);
                        }
                    }
                }
                String strUnitName = m_unitEngine.getItemById(m_unitId).getUnitName();

                m_titleTextView.setText(strUnitName);
                recreateList();

            }
        }if(resultCode==RESULT_CANCELED ){
            if(m_unitId==-1){
                finish();
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        m_menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.compose_activity_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                if(m_wordRecordInfo!=null){
                    m_wordRecordInfo.setUnitId(m_unitId);
                    m_wordEngine.deleteItem(m_wordRecordInfo);
                    setInsetTegToAddButton();
                    recreateList();
                    clearEditText();
                    return true;
                }

            default:

                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(R.id.tools_group,m_addAndUpdateButton.getTag().toString().equals(UPDATE_TAG));
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        mProgressBar.setVisibility(View.VISIBLE);

        String language = "en-ru";//getLanguage();

        Uri baseUri = Uri.parse(YANDEX_REQUEST_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("key", KEY);
        uriBuilder.appendQueryParameter("text", m_originalWordEditText.getText().toString());
        uriBuilder.appendQueryParameter("lang", language);

        return  new TranslateLoader(this,uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String strDescription) {
        mProgressBar.setVisibility(View.INVISIBLE);


        WordRecordInfo wordRecordInfo = new WordRecordInfo(m_originalWordEditText.getText().toString(), strDescription,m_unitId);

        if(m_addAndUpdateButton.getTag()== INSERT_TAG){
            m_wordEngine.insertItem(wordRecordInfo);
        }else if(m_addAndUpdateButton.getTag()== UPDATE_TAG){
            wordRecordInfo.setId((long) m_addAndUpdateButton.getTag(R.id.addWordButtonComposeActivity));
            m_wordEngine.updateItem(wordRecordInfo);
            //view.setTag(INSERT_TAG);
            setInsetTegToAddButton();
        }

        clearEditText();
        recreateList();

    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    /*Method return true if device has internet connection*/
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}