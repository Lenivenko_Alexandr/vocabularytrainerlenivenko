package com.lenivenco.as.vocabularytrainer.constants;


public final class UnitsDBConstants  {

    public static final String DB_NAME = "VOCABULARY_TRAINER_DB";
    public static final String INTEGER_PRIMARY_KEY_AUTOINCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT, ";

    public static final class UnitsTable{

        public static final String TABLE_NAME = "UNITS_TABLE_NAME";

        public static final String TABLE_FIELD_ID = "_unit_primary_id";
        public static final String TABLE_FIELD_UNIT_NAME = "_unit_name";
        public static final String TABLE_FIELD_UNIT_CREATE_TIME = "_create_time";
        public static final String TABLE_FIELD_UNIT_UPDATE_TIME = "_update_time";
        public static final String TABLE_FIELD_UNIT_lAST_USE_TIME = "_last_use_time";

    }

    public static final class WordsTable{

        public static final String TABLE_NAME = "WORDS_TABLE_NAME";

        public static final String TABLE_FIELD_ID = "_word_primary_id";
        public static final String TABLE_FIELD_ORIGINAL_WORD = "_original_word";
        public static final String TABLE_FIELD_DESCRIPTION_WORD = "_description_word";
        public static final String TABLE_FIELD_UNIT_IS_WORD_LEARNED = "_is_word_learned";

    }

    public static final class LinkUnitsTableWordsTable{

        public static final String TABLE_NAME = "LINT_UNITS_TABLE_WORDS_TABLE";

        public static final String TABLE_FIELD_ID = "_link_primary_id";
        public static final String TABLE_FIELD_UNIT_ID = "_unit_id";
        public static final String TABLE_FIELD_WORD_ID = "_word_id";

    }


}
