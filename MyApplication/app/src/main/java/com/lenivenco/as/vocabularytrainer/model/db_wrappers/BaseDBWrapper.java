package com.lenivenco.as.vocabularytrainer.model.db_wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.lenivenco.as.vocabularytrainer.dataBase.DBHelper;
import com.lenivenco.as.vocabularytrainer.model.Entity;
import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;

import java.util.ArrayList;

public abstract class BaseDBWrapper <T extends Entity> extends SuperBaseBDWrapper{

    public BaseDBWrapper(Context context, String strTableName) {
        super(context, strTableName);
    }

    public ArrayList<T> getAll(){
        ArrayList<T> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(),null,null,null,null,null,null,null);
        if(cursor!=null){
            try{
                if(cursor.moveToFirst()){
                    do{
                        T item = getInstance(cursor);
                        arrData.add(item);

                    }while (cursor.moveToNext());
                }
            }finally {
                cursor.close();
            }
        }
        db.close();
        return  arrData;
    }

    public T getItemById(long nId){
        T item = null;

        String strSelection = getIdFieldName() + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        SQLiteDatabase database = getReadable();

        Cursor cursor = database.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    item = getInstance(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        database.close();
        return item;
    }

    public long insertItem(T item){
        long id=-1;
        SQLiteDatabase database = getWritable();
        try {
            ContentValues contentValue = item.getContentValue();
            id = database.insert(getTableName(),null,contentValue);
        }catch (Exception e){
            Log.e("MyLog","error " + e.getMessage());
        }

        database.close();
        return id;
    }

    public long updateItem(T item){
        long id=-1;
        SQLiteDatabase database = getWritable();

        String strSelection = getIdFieldName() + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};

        ContentValues contentValue = item.getContentValue();
        id = database.update(getTableName(),contentValue,strSelection,arrSelectionArgs);

        database.close();
        return id;
    }

    public int deleteItem(T item){
        SQLiteDatabase db = getWritable();
        String strSelection = getIdFieldName() + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        int id = db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
        return id;
    }

    public ArrayList<T> getAllItemsByFieldAndValue(String strFieldName, long nId){
        ArrayList<T> arrResult = new ArrayList<>();
        SQLiteDatabase database = getReadable();

        String strSelection = strFieldName + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = database.query(getTableName(), null,strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    arrResult.add(getInstance(cursor));
                }
            } finally {
                cursor.close();
            }
        }
        database.close();
        return arrResult;
    }

    protected abstract String getIdFieldName();

    protected abstract T getInstance(Cursor cursor);
}
