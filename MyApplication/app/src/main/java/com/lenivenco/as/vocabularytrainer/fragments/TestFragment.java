package com.lenivenco.as.vocabularytrainer.fragments;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.activities.TestActivity;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Handler;

public class TestFragment extends Fragment implements View.OnClickListener {

    private static final String KEY_ID = "KEY_ID";
    private static final String KEY_PARCELABLE = "KEY_PARCELABLE";

    private WordRecordInfoForTest m_wordRecordInfoForTest = null;
    ArrayList<Button> m_arrButton = null;
    private TextView m_originalWordTextView = null;
    private Button m_answerOneButton = null;
    private Button m_answerTwoButton = null;
    private Button m_answerTreeButton = null;
    private TextView m_doNotNowTextView = null;
    private WordEngine m_wordEngine= null;
    private LinearLayout m_answerLinearLayout = null;
    private ImageView m_imagePlusOne = null;
    private long m_wordId =-1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        findAllView(view);
        m_arrButton = new ArrayList<>();
        fillInArrButtonView();

        m_wordEngine = new WordEngine(getContext());
        return view;
    }

    private void fillInArrButtonView() {
        m_arrButton.add(m_answerTwoButton);
        m_arrButton.add(m_answerTreeButton);
        m_arrButton.add(m_answerOneButton);
    }

    private void findAllView(View view) {
        m_originalWordTextView = (TextView) view.findViewById(R.id.originalWordTextViewTestFragment);
//        m_originalWordTextView.setOnClickListener(this);

        m_answerOneButton = (Button) view.findViewById(R.id.answer1WordButtonTestFragment);
        m_answerOneButton.setOnClickListener(this);

        m_answerTwoButton = (Button) view.findViewById(R.id.answer2WordButtonTestFragment);
        m_answerTwoButton.setOnClickListener(this);

        m_answerTreeButton = (Button) view.findViewById(R.id.answer3WordButtonTestFragment);
        m_answerTreeButton.setOnClickListener(this);

        m_doNotNowTextView = (TextView) view.findViewById(R.id.doNotNowTextViewTestFragment);
        m_doNotNowTextView.setOnClickListener(this);

        m_answerLinearLayout = (LinearLayout) view.findViewById(R.id.answerLinearLayoutTestFragment);

        m_imagePlusOne = (ImageView) view.findViewById(R.id.imagePlusOneTestFragment);


    }

    @Override
    public void onClick(View view) {
        int xValue=0;
        int yValue=0;
        if(view.getId()==R.id.doNotNowTextViewTestFragment){
            //m_wordRecordInfoForTest.setLearntWord(false);
            m_wordRecordInfoForTest.setNullNumberTrueAnswer();
        }else if((boolean)view.getTag()){
            //m_wordRecordInfoForTest.setLearntWord(true);
            m_wordRecordInfoForTest.increaseNumberTrueAnswer();
            m_originalWordTextView.setTextColor(Color.GREEN);
            m_imagePlusOne.setVisibility(View.VISIBLE);
            xValue = m_answerLinearLayout.getWidth() - m_imagePlusOne.getWidth();
            m_imagePlusOne.animate().x(xValue).y(yValue);
            m_imagePlusOne.animate().alpha(0);
            m_imagePlusOne.animate().setDuration(1000);
            m_imagePlusOne.animate().start();
//            m_imagePlusOne.
            Log.d("MyLog","true");
        }else {
            xValue = m_answerLinearLayout.getWidth() - m_originalWordTextView.getWidth();
            yValue = m_answerLinearLayout.getHeight() - m_answerOneButton.getHeight()- m_answerTwoButton.getHeight()- m_answerTreeButton.getHeight();
            yValue = m_answerOneButton.getHeight()+ m_answerTwoButton.getHeight() + m_answerTreeButton.getHeight();
            m_originalWordTextView.animate().rotation(180f);
            m_originalWordTextView.animate().x(xValue).y(yValue);


            ((Button)view).setTextColor(Color.RED);
        }
        m_wordEngine.updateItem(m_wordRecordInfoForTest);
        m_originalWordTextView.animate().setStartDelay(500);
        m_originalWordTextView.animate().alpha(0);

        m_originalWordTextView.animate().withEndAction(new Runnable() {
            @Override
            public void run() {
                ((TestActivity)getActivity()).nextFragment();
            }
        }).start();




    }

    @Override
    public void onResume() {
        super.onResume();
        if(m_wordRecordInfoForTest==null){
            WordEngine wordEngine = new WordEngine(getContext());
            //m_wordRecordInfoForTest = wordEngine(m_wordId);
        }
        m_originalWordTextView.setText(m_wordRecordInfoForTest.getOriginalWord());

        int nPos = 0;

        for(int i = m_arrButton.size()-1; i>0; i--){
            nPos = new Random().nextInt(m_arrButton.size());
            m_arrButton.get(nPos).setText(m_wordRecordInfoForTest.getFakeAnswer()[i-1]);
            m_arrButton.get(nPos).setTag(false);
            m_arrButton.remove(nPos);
        }
        m_arrButton.get(0).setText(m_wordRecordInfoForTest.getDescription());
        m_arrButton.get(0).setTag(true);
}


    public void setWordInfo(WordRecordInfoForTest wordRecordInfoForTest) {
        m_wordRecordInfoForTest = wordRecordInfoForTest;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_PARCELABLE,m_wordRecordInfoForTest);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            m_wordRecordInfoForTest = savedInstanceState.getParcelable(KEY_PARCELABLE);
        }

    }



}
