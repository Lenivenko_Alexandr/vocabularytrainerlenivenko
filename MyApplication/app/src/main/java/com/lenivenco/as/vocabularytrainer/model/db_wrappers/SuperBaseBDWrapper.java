package com.lenivenco.as.vocabularytrainer.model.db_wrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.lenivenco.as.vocabularytrainer.dataBase.DBHelper;

public abstract class SuperBaseBDWrapper {

    //public Context m_context;
    private DBHelper m_DbHelper = null;
    private String m_strTableName = "";

    public SuperBaseBDWrapper(Context context, String strTableName){
        //m_context = context;
        m_DbHelper = new DBHelper(context);
        m_strTableName = strTableName;
    }


    protected SQLiteDatabase getReadable(){
        return m_DbHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritable(){
        return m_DbHelper.getWritableDatabase();
    }

    protected String getTableName(){
        return m_strTableName;
    }

    protected abstract String getIdFieldName();
}
