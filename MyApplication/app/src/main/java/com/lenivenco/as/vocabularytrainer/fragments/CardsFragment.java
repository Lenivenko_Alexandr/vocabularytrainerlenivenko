package com.lenivenco.as.vocabularytrainer.fragments;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;


public class CardsFragment extends Fragment {


    private static final String KEY_ID = "KEY_ID";
    private  TextView m_wordTextView;
    private WordRecordInfo m_wordRecord;
    private long m_wordId = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cards, container, false);
        m_wordTextView = (TextView) view.findViewById(R.id.mainWordCardsFragment);
        m_wordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView textView = (TextView) view;
                if(textView.getText().equals(m_wordRecord.getOriginalWord())){
                    textView.setText(m_wordRecord.getDescription());
                }else {
                    textView.setText(m_wordRecord.getOriginalWord());

                }
                textView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.turn_sheet_animation));
            }
        });


        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        if(m_wordRecord==null){
            WordEngine wordEngine = new WordEngine(getContext());
            m_wordRecord = wordEngine.getItemById(m_wordId);
        }
        m_wordTextView.setText(m_wordRecord.getOriginalWord());

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(KEY_ID,m_wordId);
        super.onSaveInstanceState(outState);
    }

    public void setWordRecordId(long wordRecordId) {
        this.m_wordId = wordRecordId;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            if(m_wordId==-1){
                m_wordId = (long) savedInstanceState.getLong(KEY_ID);
            }

    }
}

