package com.lenivenco.as.vocabularytrainer.model.engines;

import android.content.Context;

import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.LinkDBWrapper;


import java.util.ArrayList;

public class LinkEngine {
    private Context m_context = null;

    public LinkEngine(Context context){
        m_context = context;
    }

    public ArrayList<LinkRecordInfo> getAll(){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return linkDBWrapper.getAll();
    }

    public LinkRecordInfo getItemById(long nId){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return linkDBWrapper.getItemById(nId);
    }


    public long insertItem(LinkRecordInfo item){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        long nId = linkDBWrapper.insertItem(item);
        return nId;
    }

    public void updateItem(LinkRecordInfo item){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        linkDBWrapper.updateItem(item);
    }

    public int deleteItem(LinkRecordInfo item){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return linkDBWrapper.deleteItem(item);
    }

    public ArrayList<LinkRecordInfo> getAllConnectOnlyOneUnit(long nId){
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return  linkDBWrapper.getAllConnectOnlyOneUnit(nId);

    }

    public int deleteItemsByUnitId(long unitId) {
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return linkDBWrapper.deleteItemsByUnitId(unitId);
    }

    public boolean isWordForOneUnit(WordRecordInfo item) {
        LinkDBWrapper linkDBWrapper = new LinkDBWrapper(m_context);
        return linkDBWrapper.isWordForOneUnit(item);
    }
}
