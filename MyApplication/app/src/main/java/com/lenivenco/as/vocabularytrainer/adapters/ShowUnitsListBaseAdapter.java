package com.lenivenco.as.vocabularytrainer.adapters;

import android.content.Context;
import android.icu.util.Calendar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ShowUnitsListBaseAdapter extends BaseAdapter {

    private final Context m_context;
    ArrayList<UnitRecordInfo> arrayData = null;

    public ShowUnitsListBaseAdapter(Context context, ArrayList<UnitRecordInfo> arrayData){
        m_context = context;
        this.arrayData = arrayData;
    }
    @Override
    public int getCount() {
        return arrayData.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((UnitRecordInfo)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_unit_base_info_for_activity_main, viewGroup, false);
        }
        UnitRecordInfo unitRecordInfo = ((UnitRecordInfo)getItem(position));

        TextView unitNameTextView = (TextView)view.findViewById(R.id.unitNameTextViewItemBaseInfo);
        unitNameTextView.setText(unitRecordInfo.getUnitName());

        TextView unitPercentTextView = (TextView)view.findViewById(R.id.unitPercentTextViewItemBaseInfo);
        int nPercent = unitRecordInfo.getPercent();

        if(0<=nPercent && nPercent<=30){
            unitPercentTextView.setTextColor(m_context.getResources().getColor( R.color.colorErrorMessageTextView));
        }else if(31<=nPercent && nPercent<=70){
            unitPercentTextView.setTextColor(m_context.getResources().getColor( R.color.colorBackgroundItemIsNotLearn));
        }else {
            unitPercentTextView.setTextColor(m_context.getResources().getColor( R.color.colorBackgroundItemIsLearn));
        }
        unitPercentTextView.setText(nPercent+"%");


        TextView unitCountWordsTextView = (TextView)view.findViewById(R.id.unitCountWordTextViewItemBaseInfo);
        unitCountWordsTextView.setText(unitRecordInfo.getLearntWords()+"/"+unitRecordInfo.getAllWords());

        TextView unitCreateTimeTextView = (TextView)view.findViewById(R.id.unitCreateTimeTextViewItemBaseInfo);
//        final String timeString =  new SimpleDateFormat("dd-MM-yyyy ' ' HH:mm").format(unitRecordInfo.getCreateDateTime()*(long)1000);
        final String timeString =  new SimpleDateFormat("dd-MM-yyyy").format(unitRecordInfo.getCreateDateTime()*(long)1000);
        unitCreateTimeTextView.setText(timeString);
        return view;
    }
}
