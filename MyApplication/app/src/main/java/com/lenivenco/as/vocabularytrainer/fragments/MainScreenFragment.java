package com.lenivenco.as.vocabularytrainer.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.activities.ComposeActivity;
import com.lenivenco.as.vocabularytrainer.activities.LearnActivity;
import com.lenivenco.as.vocabularytrainer.activities.MainActivity;
import com.lenivenco.as.vocabularytrainer.activities.TestActivity;
import com.lenivenco.as.vocabularytrainer.adapters.ShowUnitsListBaseAdapter;
import com.lenivenco.as.vocabularytrainer.dialogs.EditUnitDialog;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.engines.UnitEngine;

import java.util.ArrayList;

public class  MainScreenFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    public static final String EDIT_ACTIVITY_KEY = "EDIT_ACTIVITY_KEY";
    public static final String LEARN_ACTIVITY_KEY = "LEARN_ACTIVITY_KEY";
    public static final String TEST_ACTIVITY_KEY = "TEST_ACTIVITY_KEY";
    private static final String KEY_STRING = "KEY_STRING";

    private final int CONTEXT_MENU_RENAME = 111;
    private final int CONTEXT_MENU_REMOVE = 222;

    private ListView m_unitsListView = null;
    private UnitEngine m_unitEngine = null;
    private ArrayList<UnitRecordInfo> m_arrayData = null;
    private ShowUnitsListBaseAdapter m_showUnitsListBaseAdapter = null;
    private String m_strKey = null;
    private View m_globalTestUnitButton = null;
    private View m_addUnitFloatingActionButton = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);

        m_unitsListView = (ListView) view.findViewById(R.id.unitsListViewMainScreenFragment);
        m_unitsListView.setOnItemClickListener(this);
        //m_unitsListView.setOnItemLongClickListener(this);

        m_addUnitFloatingActionButton = view.findViewById(R.id.addNewUnitFloatingActionButtonMainScreenFragment);
        m_addUnitFloatingActionButton.setOnClickListener(this);
        m_globalTestUnitButton = view.findViewById(R.id.globalTestButtonMainScreenFragment);
        m_globalTestUnitButton.setOnClickListener(this);


        m_unitEngine = new UnitEngine(getContext());
        m_arrayData = m_unitEngine.getAll();
        registerForContextMenu(m_unitsListView);
        m_showUnitsListBaseAdapter = new ShowUnitsListBaseAdapter(getContext(), m_arrayData);
        m_unitsListView.setAdapter(m_showUnitsListBaseAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        printList();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addNewUnitFloatingActionButtonMainScreenFragment:

                Intent intent = new Intent(getContext(),ComposeActivity.class);
                intent.putExtra(ComposeActivity.KEY_ID,-1); // start compose activity for create new unit because id equal -1;
                startActivity(intent);
                break;

            case R.id.globalTestButtonMainScreenFragment:
                Intent globalTestIntent = new Intent(getContext(),TestActivity.class);
                globalTestIntent.putExtra(ComposeActivity.KEY_ID,-1);
                startActivity(globalTestIntent);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        m_unitEngine.getItemById(id).setLastUsedDateTime();

        Intent intent = null;
        switch (m_strKey){
            case EDIT_ACTIVITY_KEY:
                intent = new Intent(getContext(),ComposeActivity.class);
                intent.putExtra(ComposeActivity.KEY_ID,id);
                break;
            case LEARN_ACTIVITY_KEY:
                intent = new Intent(getContext(),LearnActivity.class);
                intent.putExtra(ComposeActivity.KEY_ID,id);
                break;
            case TEST_ACTIVITY_KEY:
                intent = new Intent(getContext(),TestActivity.class);
                intent.putExtra(ComposeActivity.KEY_ID,id);
                break;

        }

        startActivity(intent);
    }


    private void printList() {

            if(m_strKey==EDIT_ACTIVITY_KEY){
                m_addUnitFloatingActionButton.setVisibility(View.VISIBLE);
            }
            if(m_strKey==TEST_ACTIVITY_KEY){
                m_globalTestUnitButton.setVisibility(View.VISIBLE);
            }
            m_arrayData.clear();
            m_arrayData.addAll(m_unitEngine.getAll());
            m_showUnitsListBaseAdapter.notifyDataSetChanged();

    }

    public void setKey(String key) {
        this.m_strKey = key;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long nId) {
        /*((MainActivity)getActivity()).on*/
        return true;
    }

     @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.unitsListViewMainScreenFragment) {
                menu.setHeaderTitle(getResources().getString(R.string.setting));

                menu.add(Menu.NONE, CONTEXT_MENU_RENAME, 1, getResources().getString(R.string.rename));
                menu.add(Menu.NONE, CONTEXT_MENU_REMOVE, 2, getResources().getString(R.string.remove));

        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = null;
        try {
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        } catch (ClassCastException e) {
            Log.e("MyLog", "bad menuInfo", e);
            return false;
        }

        final UnitRecordInfo unitRecordInfo = (UnitRecordInfo) m_showUnitsListBaseAdapter.getItem(info.position);
        switch (item.getItemId()) {

            case CONTEXT_MENU_REMOVE:
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCancelable(false);
                alertDialog.setTitle(getResources().getString(R.string.remove) + " " + unitRecordInfo.getUnitName());
                alertDialog.setMessage(getResources().getString(R.string.remove_message));
                alertDialog.setPositiveButton(getResources().getString(R.string.remove), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        m_unitEngine.deleteItem(unitRecordInfo);
                        printList();
                    }
                });

                alertDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alertDialog.setMessage(getResources().getString(R.string.remove_message));
                alertDialog.create().show();

                ((MainActivity)getActivity()).setPagerAdapter();
                return true;

            case CONTEXT_MENU_RENAME:
                /*Intent intent = new Intent(getContext(), EditUnitDialog.class);
                intent.putExtra(ComposeActivity.KEY_ID,unitRecordInfo.getId());
                startActivity(intent);*/
                createDialog(unitRecordInfo.getId());
                printList();
                return true;

        }
        return super.onContextItemSelected(item);
    }

    public void createDialog(long nUnitId) {
        EditUnitDialog editUnitDialog = new EditUnitDialog(getContext(),nUnitId);
        editUnitDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                printList();
            }
        });
        editUnitDialog.setCancelable(false);
        editUnitDialog.setTitle(getResources().getString(R.string.setName));
        editUnitDialog.show();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_STRING,m_strKey);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(m_strKey==null){
            m_strKey = savedInstanceState.getString(KEY_STRING);
        }

    }
}
