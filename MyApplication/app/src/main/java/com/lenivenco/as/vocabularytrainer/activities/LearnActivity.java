package com.lenivenco.as.vocabularytrainer.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.adapters.CardsPagerAdapter;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import java.util.ArrayList;

public class LearnActivity extends AppCompatActivity {

    private ViewPager viewPager = null;
    private TabLayout tabLayout = null;
    private long m_unitId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);
        drawToolBar();

        Intent intent  = getIntent();

        if(!intent.equals(null)){
            Bundle bundle = intent.getExtras();
            if (!bundle.equals(null)){
                m_unitId = (long) bundle.getLong(ComposeActivity.KEY_ID,-1);

            }
        }

        startLearn();

        tabLayout = (TabLayout) findViewById(R.id.layoutTabStripLearnActivity);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void startLearn() {
        ArrayList< WordRecordInfo> arrWordRecord = new WordEngine(this).getAllByUnitId(m_unitId);
        viewPager = (ViewPager) findViewById(R.id.viewPagerLearnActivity);
        viewPager.setAdapter(new CardsPagerAdapter(getSupportFragmentManager(),arrWordRecord));
    }

    private void drawToolBar() {
        Toolbar m_toolbar = (Toolbar) findViewById(R.id.toolbarLearnActivity);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle(getResources().getString(R.string.learn_activity_name));


    }
}
