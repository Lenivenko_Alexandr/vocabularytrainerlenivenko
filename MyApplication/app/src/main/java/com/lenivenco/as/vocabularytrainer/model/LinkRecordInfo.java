package com.lenivenco.as.vocabularytrainer.model;


import android.content.ContentValues;
import android.database.Cursor;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;

public class LinkRecordInfo extends Entity{


    private long m_nUnitId = -1;
    private long m_nWordId = -1;

    public LinkRecordInfo(long nUnitID, long nWordId){
        m_nUnitId = nUnitID;
        m_nWordId = nWordId;

    }

    public LinkRecordInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_ID)));
        setUnitId(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID)));
        setWordId(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID)));
    }

    @Override
    public ContentValues getContentValue() {
        ContentValues values = new ContentValues();
        values.put(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID, getUnitId());
        values.put(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID,getWordId());

        return values;
    }

    public long getUnitId() {
        return m_nUnitId;
    }

    public void setUnitId(long m_nUnitId) {
        this.m_nUnitId = m_nUnitId;
    }

    public long getWordId() {
        return m_nWordId;
    }

    public void setWordId(long m_nWordId) {
        this.m_nWordId = m_nWordId;
    }


}
