package com.lenivenco.as.vocabularytrainer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.activities.LearnActivity;
import com.lenivenco.as.vocabularytrainer.activities.MainActivity;

public class RepeatAgainFragment extends Fragment implements View.OnClickListener {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repeat_again, container, false);
        View m_repeatAgainButton = view.findViewById(R.id.repeatAgainButtonRepeatAgainFragment);
        m_repeatAgainButton.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {
        ((LearnActivity)getActivity()).startLearn();
    }
}
