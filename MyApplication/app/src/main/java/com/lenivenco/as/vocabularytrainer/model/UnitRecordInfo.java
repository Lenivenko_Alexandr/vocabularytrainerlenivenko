package com.lenivenco.as.vocabularytrainer.model;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.Build;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;


public class UnitRecordInfo extends Entity{

    private String m_strUnitName = "";
    private int m_nLearntWords = 0;
    private int m_nAllWords = 0;
    //private int m_nPercent = 0;
    private long m_nCreateDateTime =0;
    private long m_nUpdateDateTime =0;
    private long m_nLastUsedDateTime =0;

    public UnitRecordInfo(String strUnitName){
        setId(-1);
        setUnitName(strUnitName);
        setLearntWords(0);
        setAllWords(0);
        //m_nPercent = countPercent();
        //m_nCreateDateTime = m_nUpdateDateTime = m_nLastUsedDateTime = 0;
        m_nCreateDateTime = m_nUpdateDateTime = m_nLastUsedDateTime = writeDateTime();

    }



    public UnitRecordInfo(String strUnitName, int nLearntWords, int nAllWords){
        setId(-1);
        setUnitName(strUnitName);
        setLearntWords(nLearntWords);
        setAllWords(nAllWords);

        //m_nPercent = countPercent();
        //writeDateTime();

    }

    public UnitRecordInfo(long nId, String strUnitName, int nLearntWords, int nAllWords){

        this(strUnitName,nLearntWords,nAllWords);
        setId(nId);
    }

    public UnitRecordInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.UnitsTable.TABLE_FIELD_ID)));
        setUnitName(cursor.getString(cursor.getColumnIndex(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_NAME)));

        setCreateDataTime(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_CREATE_TIME)));
        setUpdateDateTime(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_UPDATE_TIME)));
        setLastUsedDateTime(cursor.getLong(cursor.getColumnIndex(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_lAST_USE_TIME)));


        //m_nLearntWords = 0;
        //m_nAllWords = 0;
        //m_nPercent = countPercent();

    }

    @TargetApi(Build.VERSION_CODES.N)
    private long writeDateTime() {

        return (long) System.currentTimeMillis()/1000;
    }

     //method for count how many percent of words from unit is learned
    private int countPercent(){


        float fResult=0;
        if(m_nAllWords!=0){
            fResult = ((float)m_nLearntWords/(float) m_nAllWords)*100;
        }else {
            fResult=0;
        }

        return (int)fResult;
    }


    @Override
    public ContentValues getContentValue() {
        ContentValues values = new ContentValues();
        //values.put(UnitsDBConstants.UnitsTable.TABLE_FIELD_ID,getId());
        values.put(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_NAME,getUnitName());
        values.put(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_CREATE_TIME,getCreateDateTime());
        values.put(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_UPDATE_TIME,getUpdateDateTime());
        values.put(UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_lAST_USE_TIME,getLastUsedDateTime());

        return values;
    }

    public String getUnitName() {
        return m_strUnitName;
    }

    public void setUnitName(String m_strUnitName) {
        this.m_strUnitName = m_strUnitName.trim();
    }

    public int getLearntWords() {
        return m_nLearntWords;
    }

    public void setLearntWords(int m_nLearntWords) {
        this.m_nLearntWords = m_nLearntWords;
    }

    public int getAllWords() {
        return m_nAllWords;
    }

    public void setAllWords(int m_nAllWords) {
        this.m_nAllWords = m_nAllWords;
    }

    public int getPercent(){
        return countPercent();
        //return m_nPercent;
    }

    public long getCreateDateTime() {
        return m_nCreateDateTime;
    }

    public void setCreateDataTime(long time){
        m_nCreateDateTime = time;
    }

    public long getUpdateDateTime() {
        return m_nUpdateDateTime;
    }

    public void setUpdateDateTime(long m_nUpdateDateTime) {
        this.m_nUpdateDateTime = m_nUpdateDateTime;
    }

    public void setUpdateDateTime() {
        this.m_nUpdateDateTime = writeDateTime();
    }

    public long getLastUsedDateTime() {
        return m_nLastUsedDateTime;
    }

    public void setLastUsedDateTime(long m_nLastUsedDateTime) {
        this.m_nLastUsedDateTime = m_nLastUsedDateTime;
    }

    public void setLastUsedDateTime() {
        this.m_nLastUsedDateTime = writeDateTime();
    }


}
