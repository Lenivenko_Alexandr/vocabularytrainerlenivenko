package com.lenivenco.as.vocabularytrainer.model.engines;

import android.content.Context;
import android.util.Log;

import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.FakeWordDBWrapper;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.LinkDBWrapper;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.UnitDBWrapper;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.WordDBWrapper;

import java.util.ArrayList;

public class WordEngine {

    private Context m_context = null;

    public WordEngine(Context context){
        m_context = context;
    }

    public long insertItem(WordRecordInfo item){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);

        long nWordId = wrapper.insertItem(item);
        if(nWordId!=-1){
            LinkEngine linkEngine = new LinkEngine(m_context);
            linkEngine.getAllConnectOnlyOneUnit(item.getUnitId());
            linkEngine.insertItem(new LinkRecordInfo(item.getUnitId(),nWordId));
        }

        return nWordId;
    }

    public WordRecordInfo getItemById(long nId){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
        return wrapper.getItemById(nId);
    }



    public ArrayList<WordRecordInfo> getAllByUnitId(long nId){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
        return wrapper.getAllByUnitId(nId);
    }

    public void updateItem(WordRecordInfo item){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
        wrapper.updateItem(item);
    }

    public int deleteItem(WordRecordInfo item){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
        LinkEngine linkEngine = new LinkEngine(m_context);

        if(linkEngine.isWordForOneUnit(item)){
            linkEngine.deleteItem(new LinkRecordInfo(item.getUnitId(),item.getId()));
            return wrapper.deleteItem(item);
        }
        linkEngine.deleteItem(new LinkRecordInfo(item.getUnitId(),item.getId()));
        return 0;
    }

    public void addWordInfoToUnit(ArrayList<UnitRecordInfo> arrUnitRecordInfo){
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
            wrapper.addWordInfoToUnit(arrUnitRecordInfo);
        }


    public int deleteItems(ArrayList<LinkRecordInfo> arrLinkRecords) {
        WordDBWrapper wrapper = new WordDBWrapper(m_context);

        return wrapper.deleteItems(arrLinkRecords);
    }

    public ArrayList<WordRecordInfo> getAll() {
        WordDBWrapper wrapper = new WordDBWrapper(m_context);
        return wrapper.getAll();
    }


    public ArrayList<WordRecordInfoForTest> createArrayForTest(long m_unitId) {

            ArrayList<WordRecordInfoForTest> wordRecords = new ArrayList<>();
            ArrayList<WordRecordInfo> arrWordRecord = null;

            if(m_unitId==-1){
                arrWordRecord = getAll();
            }else {
               arrWordRecord = getAllByUnitId(m_unitId);
            }

            FakeWordDBWrapper fakeWordDBWrapper = new FakeWordDBWrapper(m_context);
            for(WordRecordInfo record : arrWordRecord){
                record.setUnitId(m_unitId);
                WordRecordInfoForTest testRecord = new WordRecordInfoForTest(record,2);
                testRecord.setFakeAnswer(fakeWordDBWrapper.getFakeAnswer(2,testRecord.getId()));
                wordRecords.add(testRecord);

            }
            return wordRecords;

    }
}
