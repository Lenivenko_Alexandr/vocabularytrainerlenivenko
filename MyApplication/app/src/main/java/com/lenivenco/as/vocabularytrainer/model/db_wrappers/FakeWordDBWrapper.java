package com.lenivenco.as.vocabularytrainer.model.db_wrappers;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;
import com.lenivenco.as.vocabularytrainer.model.Entity;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;

import java.util.ArrayList;
import java.util.Random;

public class FakeWordDBWrapper extends BaseDBWrapper<WordRecordInfoForTest>{

    public FakeWordDBWrapper(Context context) {
        super(context, UnitsDBConstants.WordsTable.TABLE_NAME);
    }

    @Override
    protected String getIdFieldName() {
        return UnitsDBConstants.WordsTable.TABLE_FIELD_ID;
    }

    @Override
    protected WordRecordInfoForTest getInstance(Cursor cursor) {
        return new WordRecordInfoForTest(cursor);
    }


    public long getMaxWordId(){
        SQLiteDatabase db = getReadable();
        long id = -1;
        Cursor cursor = db.rawQuery("SELECT MAX(" +getIdFieldName() + ") FROM " +getTableName() +";", null);
        if(cursor.moveToFirst()){
            String result =  cursor.getString(0);
            id = Long.parseLong(result);
        }else {
            return -1;
        }


        return id;
    }

    public String [] getFakeAnswer(int nCount, long nOriginalId){
        String[] arrStrFakeWord = new String[nCount];
        long randomId = 0;
        int i=0;
        Random random = new Random();
        do{
            randomId = random.nextInt((int)getMaxWordId());
            WordRecordInfoForTest  itemById = getItemById(randomId);
            if(itemById!=null){
                if(nOriginalId!=itemById.getId()){
                    if(i==0){
                        arrStrFakeWord[i] = itemById.getDescription();
                        nCount--;
                        i++;
                    }else {
                        boolean bFlag = true;
                        for(int j=0;j<i;j++){
                            if(arrStrFakeWord[j].equals(itemById.getDescription())){
                                bFlag = false;
                            }
                        }if(bFlag){
                            arrStrFakeWord[i] = itemById.getDescription();
                            nCount--;
                            i++;
                        }
                    }

                }


            }

        }while (nCount!=0);
        return arrStrFakeWord;
    }


}
