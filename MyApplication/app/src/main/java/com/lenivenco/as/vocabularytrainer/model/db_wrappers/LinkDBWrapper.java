package com.lenivenco.as.vocabularytrainer.model.db_wrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;
import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;

import java.util.ArrayList;

public class LinkDBWrapper extends BaseDBWrapper<LinkRecordInfo> {

    public LinkDBWrapper(Context context) {
        super(context, UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_NAME);
    }

    @Override
    protected String getIdFieldName() {
        return UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_ID;
    }

    @Override
    protected LinkRecordInfo getInstance(Cursor cursor) {
        return new LinkRecordInfo(cursor);
    }

    public ArrayList<LinkRecordInfo> getAllConnectOnlyOneUnit(long nId){

        ArrayList<LinkRecordInfo> arrAllLinkRecords = getAll();
        ArrayList<LinkRecordInfo> arrLinkRecordInfoConnectedOneUnit = getArrListByUnitId(nId);
        ArrayList<LinkRecordInfo> arrTemp = new ArrayList<>();

        for(int i=0;i<arrLinkRecordInfoConnectedOneUnit.size();i++){
            for(int j=0;j<arrAllLinkRecords.size();j++){
                if(arrAllLinkRecords.get(j).getWordId()==arrLinkRecordInfoConnectedOneUnit.get(i).getWordId()){
                    if(arrAllLinkRecords.get(j).getUnitId()!=arrLinkRecordInfoConnectedOneUnit.get(i).getUnitId()){
                        arrTemp.add(arrLinkRecordInfoConnectedOneUnit.get(i));
                    }
                }
            }
        }

        arrLinkRecordInfoConnectedOneUnit.removeAll(arrTemp);


        return  arrLinkRecordInfoConnectedOneUnit;

    }

    public ArrayList<LinkRecordInfo> getArrListByUnitId(long nId){
        ArrayList<LinkRecordInfo> arrLinkRecord = new ArrayList<>();

        String strSelection = UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        SQLiteDatabase database = getReadable();

        Cursor cursor = database.query(UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_NAME, null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        LinkRecordInfo item = new LinkRecordInfo(cursor);
                        arrLinkRecord.add(item);

                    }while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        database.close();
        return arrLinkRecord;
    }


    public int deleteItemsByUnitId(long unitId) {
        SQLiteDatabase db = getWritable();

        String strSelection = UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(unitId)};
        int nCount = db.delete(getTableName(),strSelection,arrSelectionArgs);

        db.close();
        return nCount;

    }

    @Override
    public int deleteItem(LinkRecordInfo item) {
        LinkRecordInfo linkRecord = null;
        if(item.getId()==-1){
            linkRecord = getItemByUnitAndWordId(item);
        }else {
            linkRecord = item;
        }
        return super.deleteItem(linkRecord);

    }

    private LinkRecordInfo getItemByUnitAndWordId(LinkRecordInfo linkRecord){
        LinkRecordInfo item = null;

        String strSelection = UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID + "=? AND " +
                UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(linkRecord.getUnitId()), Long.toString(linkRecord.getWordId())};

        SQLiteDatabase database = getReadable();

        Cursor cursor = database.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    item = getInstance(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        database.close();
        return item;
    }

    public boolean isWordForOneUnit(WordRecordInfo item) {
        ArrayList<LinkRecordInfo> arrLink = new ArrayList<>();
        boolean bFlag = false;

        String strSelection = UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};

        SQLiteDatabase database = getReadable();

        Cursor cursor = database.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor != null) {
            try {if(cursor.moveToFirst()){
                do{
                    arrLink.add(getInstance(cursor));
                }while (cursor.moveToNext());
            }

            } finally {
                cursor.close();
            }
        }
        database.close();
        if (arrLink != null) {
            if (arrLink.size() == 1) {
                bFlag = true;
            }

        }

        return bFlag;
    }

    @Override
    public long insertItem(LinkRecordInfo item) {
        LinkRecordInfo result = getItemByUnitAndWordId(item);
        if(result == null){
            return super.insertItem(item);
        }
        return result.getId();
    }

}
