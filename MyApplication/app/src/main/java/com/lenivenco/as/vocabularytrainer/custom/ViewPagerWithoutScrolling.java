package com.lenivenco.as.vocabularytrainer.custom;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class ViewPagerWithoutScrolling extends ViewPager {
        public ViewPagerWithoutScrolling(Context context) {
            super(context);
        }

        public ViewPagerWithoutScrolling(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent event) {
            // Never allow swiping to switch between pages
            return false;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            // Never allow swiping to switch between pages
            return false;
        }


    }
