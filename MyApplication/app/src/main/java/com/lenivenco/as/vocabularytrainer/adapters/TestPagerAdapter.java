package com.lenivenco.as.vocabularytrainer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.lenivenco.as.vocabularytrainer.activities.InfoTestActivity;
import com.lenivenco.as.vocabularytrainer.fragments.TestFragment;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;

import java.util.ArrayList;

public class TestPagerAdapter extends FragmentStatePagerAdapter{

    ArrayList<WordRecordInfoForTest> m_arrWordRecordInfoForTest = null;

    public TestPagerAdapter(FragmentManager fm , ArrayList<WordRecordInfoForTest> arrTitleForTabLayout) {

        super(fm);
        this.m_arrWordRecordInfoForTest = arrTitleForTabLayout;
    }


    @Override
    public int getCount() {
        return m_arrWordRecordInfoForTest.size();
    }

    @Override
    public Fragment getItem(int position) {
        TestFragment testFragment = new TestFragment();
        testFragment.setWordInfo(m_arrWordRecordInfoForTest.get(position));
        return testFragment;
    }

    public void setArray(ArrayList<WordRecordInfoForTest> array) {
        this.m_arrWordRecordInfoForTest = array;
    }

    /*@Override
    public CharSequence getPageTitle(int position) {
        String strTitle = null;
        switch (m_arrTitleForTabLayout.get(position)){public android.support.v4.app.FragmentManager getFragmentManager() {
        return fragmentManager;
    }
            case MainScreenFragment.EDIT_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.compose_activity_name);
                break;
            case MainScreenFragment.LEARN_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.learn_activity_name);
                break;
            case MainScreenFragment.TEST_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.test_activity_name);
                break;
        }
        return strTitle;
    }*/
}
