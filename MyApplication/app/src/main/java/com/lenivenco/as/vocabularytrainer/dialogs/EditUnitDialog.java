package com.lenivenco.as.vocabularytrainer.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.activities.ComposeActivity;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.engines.UnitEngine;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import java.util.ArrayList;

public class EditUnitDialog extends Dialog implements View.OnClickListener {

    public static final int REQUEST_COD_EDIT_UNIT_DIALOG = 111;
    public static final String UNIT_NAME = "UNIT_NAME";
    public static final String UNIT_NAME_FOR_EDIT_TEXT = "UNIT_NAME_FOR_EDIT_TEXT";
    private EditText m_unitNameEditText = null;
    private TextView m_errorTextView;

    String m_strOldUnitName = "";
    private long m_unitId = -1;
    private UnitEngine m_engine = null;
    private ArrayList<WordRecordInfo> m_arrWordsData = null;
    private boolean bFlag = false;

    public EditUnitDialog(Context context, long nUnitId) {
        super(context);
        m_unitId = nUnitId;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_unit);

        final View addButton = findViewById(R.id.addUnitButtonEditUnitDialog);
        addButton.setOnClickListener(this);
        View cancelButton = findViewById(R.id.cancelButtonEditUnitDialog);
        cancelButton.setOnClickListener(this);

        main();

        m_errorTextView = (TextView) findViewById(R.id.errorMessageTextViewUnitDialog);

        m_unitNameEditText = (EditText) findViewById(R.id.unitNameEditTextEditUnitDialog);
        m_unitNameEditText.setText(m_strOldUnitName);
        m_unitNameEditText.setSelection(m_strOldUnitName.length());
        m_unitNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strUnitName = editable.toString();

                if(m_engine.hasSameUnitName(strUnitName.trim())){
                    m_errorTextView.setVisibility(View.VISIBLE);
                    addButton.setEnabled(false);
                }else {
                    m_errorTextView.setVisibility(View.INVISIBLE);
                    addButton.setEnabled(true);
                }


            }
        });
    }

    private void main() {
        m_engine = new UnitEngine(getContext());

                if(m_unitId>-1){
                    m_strOldUnitName = m_engine.getItemById(m_unitId).getUnitName();

                }
                if(m_unitId==-2){
                    bFlag = true;
                }



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addUnitButtonEditUnitDialog:
                String strUnitName = m_unitNameEditText.getText().toString();
                if(m_unitNameEditText.getText().toString().isEmpty()||m_strOldUnitName==strUnitName){

                    //onBackPressed();
                }else{
                    Intent intent = new Intent();
                        if(m_unitId>-1){
                            UnitRecordInfo unitRecordInfo = m_engine.getItemById(m_unitId);
                            unitRecordInfo.setUnitName(strUnitName);
                            m_engine.updateItem(unitRecordInfo);

                        }else {
                            UnitRecordInfo unitRecordInfo = new UnitRecordInfo(strUnitName);
                            long id = m_engine.insertItem(unitRecordInfo);
                            m_unitId = id;
                            WordEngine m_wordEngine = new WordEngine(getContext());
                            m_arrWordsData = m_wordEngine.getAll();
                            if(bFlag){
                                for(WordRecordInfo infoForTest : m_arrWordsData){
                                    if(!infoForTest.isLearntWord()){
                                        infoForTest.setUnitId(id);
                                        m_wordEngine.insertItem(infoForTest);
                                    }

                                }
                            }

                            //intent.putExtra(ComposeActivity.KEY_ID,id);
                        }
                    /*setResult(RESULT_OK,intent);
                    finish();*/
                    dismiss();
                }

                break;
            case R.id.cancelButtonEditUnitDialog:
                //onBackPressed();
                cancel();
                break;

        }
    }

    public long getUnitId(){
        return m_unitId;
    }
}
