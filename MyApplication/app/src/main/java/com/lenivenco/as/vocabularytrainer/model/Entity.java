package com.lenivenco.as.vocabularytrainer.model;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class Entity {

    private long m_nId =-1;

    public long getId() {
        return m_nId;
    }

    public void setId(long id) {
        this.m_nId = id;
    }

    public abstract ContentValues getContentValue();
}
