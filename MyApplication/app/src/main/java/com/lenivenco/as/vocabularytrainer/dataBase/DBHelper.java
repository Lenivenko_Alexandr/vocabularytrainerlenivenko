package com.lenivenco.as.vocabularytrainer.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;

public class DBHelper extends SQLiteOpenHelper{
    public static final int VERSION = 1;

    public DBHelper(Context context) {
        super(context, UnitsDBConstants.DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("MyLog","onCreate");
        //Create table UNITS
        sqLiteDatabase.execSQL("CREATE TABLE " + UnitsDBConstants.UnitsTable.TABLE_NAME +
                " (" +
                        UnitsDBConstants.UnitsTable.TABLE_FIELD_ID + UnitsDBConstants.INTEGER_PRIMARY_KEY_AUTOINCREMENT +
                        UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_NAME + " TEXT NOT NULL, " +
                        UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_CREATE_TIME + " INTEGER, " +
                        UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_UPDATE_TIME + " INTEGER, " +
                        UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_lAST_USE_TIME + " INTEGER );");
        Log.d("MyLog","table UNITS was created");


        //Create table WORDS
        sqLiteDatabase.execSQL("CREATE TABLE " + UnitsDBConstants.WordsTable.TABLE_NAME +
                " (" +
                        UnitsDBConstants.WordsTable.TABLE_FIELD_ID + UnitsDBConstants.INTEGER_PRIMARY_KEY_AUTOINCREMENT +
                        UnitsDBConstants.WordsTable.TABLE_FIELD_ORIGINAL_WORD + " TEXT NOT NULL, " +
                        UnitsDBConstants.WordsTable.TABLE_FIELD_DESCRIPTION_WORD + " TEXT NOT NULL, " +
                        UnitsDBConstants.WordsTable.TABLE_FIELD_UNIT_IS_WORD_LEARNED + " INTEGER );");
        Log.d("MyLog","table WORDS was created");


        //Create table LinkUnitsTableWordsTable
        sqLiteDatabase.execSQL("CREATE TABLE " + UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_NAME +
                " (" +
                        UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_ID + UnitsDBConstants.INTEGER_PRIMARY_KEY_AUTOINCREMENT +
                        UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID + " INTEGER, " +
                        UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID + " INTEGER );");
        Log.d("MyLog","LinkUnitsTableWordsTable was created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
