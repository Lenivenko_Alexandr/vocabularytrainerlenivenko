package com.lenivenco.as.vocabularytrainer.model.engines;

import android.content.Context;
import android.util.Log;

import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.LinkDBWrapper;
import com.lenivenco.as.vocabularytrainer.model.db_wrappers.UnitDBWrapper;

import java.util.ArrayList;

public class UnitEngine {
    private Context m_context = null;

    public UnitEngine(Context context){
        m_context = context;
    }

    public ArrayList<UnitRecordInfo> getAll(){
        UnitDBWrapper  unitDBWrapper = new UnitDBWrapper(m_context);
        WordEngine wordEngine = new WordEngine(m_context);

        ArrayList<UnitRecordInfo> arrayListRecordInfo = unitDBWrapper.getAll();
        wordEngine.addWordInfoToUnit(arrayListRecordInfo);
        return arrayListRecordInfo;
    }

    public UnitRecordInfo getItemById(long nId){
        UnitDBWrapper  unitDBWrapper = new UnitDBWrapper(m_context);
        return unitDBWrapper.getItemById(nId);
    }


    public long insertItem(UnitRecordInfo item){
        UnitDBWrapper wrapper = new UnitDBWrapper(m_context);
        long nWordId = wrapper.insertItem(item);

        return nWordId;
    }

    public void updateItem(UnitRecordInfo item){
        UnitDBWrapper wrapper = new UnitDBWrapper(m_context);
        wrapper.updateItem(item);
    }

    public int deleteItem(UnitRecordInfo item){
        UnitDBWrapper wrapper = new UnitDBWrapper(m_context);
        LinkEngine linkEngine = new LinkEngine(m_context);
        WordEngine wordEngine = new WordEngine(m_context);

        ArrayList<LinkRecordInfo> arrLinkRecords = linkEngine.getAllConnectOnlyOneUnit(item.getId());
        if(arrLinkRecords.size()>0){
            int nCountDeleteWord = wordEngine.deleteItems(arrLinkRecords);
            int nCountDeleteLink = linkEngine.deleteItemsByUnitId(item.getId());
        }



        return wrapper.deleteItem(item);
    }

    public boolean hasSameUnitName(String strUnitName){
        UnitDBWrapper wrapper = new UnitDBWrapper(m_context);
        return  wrapper.hasSameUnitName(strUnitName);

    }


}
