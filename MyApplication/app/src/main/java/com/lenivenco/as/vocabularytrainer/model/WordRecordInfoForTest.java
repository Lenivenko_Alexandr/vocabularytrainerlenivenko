package com.lenivenco.as.vocabularytrainer.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Random;

public class WordRecordInfoForTest extends WordRecordInfo implements Parcelable{
    private int m_nFAkeWord = 3;
    private String [] m_arrStrFakeAnswer = null;


    public WordRecordInfoForTest(WordRecordInfo wordRecordInfo,int nFakeWord) {
        this(wordRecordInfo);
        m_arrStrFakeAnswer = new String[nFakeWord];
        m_nFAkeWord = nFakeWord;
    }

    public WordRecordInfoForTest(WordRecordInfo wordRecordInfo) {
        super(wordRecordInfo);
    }

    public WordRecordInfoForTest(Cursor cursor) {
        super(cursor);
    }

   /* parcel.writeLong(getUnitId());
    parcel.writeLong(getId());
    parcel.writeString(getOriginalWord());
    parcel.writeString(getDescription());
    parcel.writeArray(getFakeAnswer());
    parcel.writeByte((byte) (isLearntWord() ? 1 : 0));
    parcel.writeInt(getNumberTrueAnswer());
    parcel.writeInt(m_nFAkeWord);*/

    public WordRecordInfoForTest(Parcel parcel) {
        setUnitId(parcel.readLong());
        setId(parcel.readLong());
        setOriginalWord(parcel.readString());
        setDescription(parcel.readString());
        parcel.readStringArray(m_arrStrFakeAnswer);
        setLearntWord(parcel.readByte() != 0);
        setNumberTrueAnswer(parcel.readInt());
        m_nFAkeWord = parcel.readInt();

    }

    public String[] getFakeAnswer() {
        return m_arrStrFakeAnswer;
    }

    public void setFakeAnswer(ArrayList<WordRecordInfo> allWordRecords) {

        String [] strFakeAnswer = createFakeArray(allWordRecords,m_nFAkeWord);

        this.m_arrStrFakeAnswer = strFakeAnswer;
    }

    public void setFakeAnswer(String [] arrStrFakeAnswer) {
        this.m_arrStrFakeAnswer = arrStrFakeAnswer;
    }

    private String[] createFakeArray(ArrayList<WordRecordInfo> randomWordRecords, int m_nFAkeWord) {
        int position = 0;
        int i=0;
        for(;;){
            if(i==m_nFAkeWord){
                break;
            }
            position = getPosition(randomWordRecords.size());
            if(i==0&&(!randomWordRecords.get(position).equals(getDescription()))){
                m_arrStrFakeAnswer[i] = randomWordRecords.get(position).getDescription();
                i++;
            }else if(i>0){
                boolean bFlag = false;
                for(int j = 0; j<i; j++){
                    if(m_arrStrFakeAnswer[j].equals(randomWordRecords.get(position).getDescription())){
                        bFlag = true;
                    }
                }
                if(!bFlag){
                    m_arrStrFakeAnswer[i] = randomWordRecords.get(position).getDescription();
                    i++;
                }
            }
        }

        return m_arrStrFakeAnswer;
    }

    private int getPosition(int size) {
        return  new Random().nextInt(size);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(getUnitId());
        parcel.writeLong(getId());
        parcel.writeString(getOriginalWord());
        parcel.writeString(getDescription());
        parcel.writeStringArray(getFakeAnswer());
        parcel.writeByte((byte) (isLearntWord() ? 1 : 0));
        parcel.writeInt(getNumberTrueAnswer());
        parcel.writeInt(m_nFAkeWord);
    }

    public static final Parcelable.Creator<WordRecordInfoForTest> CREATOR
            = new Parcelable.Creator<WordRecordInfoForTest>() {
        public WordRecordInfoForTest createFromParcel(Parcel in) {
            return new WordRecordInfoForTest(in);
        }

        public WordRecordInfoForTest[] newArray(int size) {
            return new WordRecordInfoForTest[size];
        }
    };
}
