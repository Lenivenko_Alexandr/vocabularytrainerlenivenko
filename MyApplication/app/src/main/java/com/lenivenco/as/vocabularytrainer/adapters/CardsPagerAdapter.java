package com.lenivenco.as.vocabularytrainer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.lenivenco.as.vocabularytrainer.fragments.CardsFragment;
import com.lenivenco.as.vocabularytrainer.fragments.RepeatAgainFragment;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;

import java.util.ArrayList;

public class CardsPagerAdapter extends FragmentPagerAdapter{
    ArrayList<WordRecordInfo> m_arrWordRecord = null;

    public CardsPagerAdapter(FragmentManager fm ,ArrayList< WordRecordInfo> arrWordRecord) {
        super(fm);
        this.m_arrWordRecord = arrWordRecord;

    }


    @Override
    public int getCount() {
        return m_arrWordRecord.size()+1;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==m_arrWordRecord.size()){
            return new RepeatAgainFragment();
        }
        CardsFragment cardsFragment = new CardsFragment();
        cardsFragment.setWordRecordId(m_arrWordRecord.get(position).getId());
        return cardsFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "*";
    }
}
