package com.lenivenco.as.vocabularytrainer.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.fragments.MainScreenFragment;


import java.util.ArrayList;

public class MainScreenPagerAdapter extends FragmentPagerAdapter{
    private Context m_context = null;
    ArrayList<String> m_arrTitleForTabLayout = null;

    public MainScreenPagerAdapter(Context context, FragmentManager fm , ArrayList< String> arrTitleForTabLayout) {

        super(fm);
        this.m_arrTitleForTabLayout = arrTitleForTabLayout;
        this.m_context = context;

    }


    @Override
    public int getCount() {
        return m_arrTitleForTabLayout.size();
    }

    @Override
    public Fragment getItem(int position) {
        MainScreenFragment mainScreenFragment = new MainScreenFragment();
        mainScreenFragment.setKey(m_arrTitleForTabLayout.get(position));
        return mainScreenFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String strTitle = null;
        switch (m_arrTitleForTabLayout.get(position)){
            case MainScreenFragment.EDIT_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.compose_activity_name);
                break;
            case MainScreenFragment.LEARN_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.learn_activity_name);
                break;
            case MainScreenFragment.TEST_ACTIVITY_KEY:
                strTitle = m_context.getResources().getString(R.string.test_activity_name);
                break;
        }
        return strTitle;
    }



}

