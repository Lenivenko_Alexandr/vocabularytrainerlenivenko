package com.lenivenco.as.vocabularytrainer.model.db_wrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;
import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;

import java.util.ArrayList;

public class WordDBWrapper extends BaseDBWrapper<WordRecordInfo> {

    private Context m_context;

    public WordDBWrapper(Context context) {

        super(context, UnitsDBConstants.WordsTable.TABLE_NAME);
        m_context = context;
    }

    @Override
    protected String getIdFieldName() {
        return UnitsDBConstants.WordsTable.TABLE_FIELD_ID;
    }

    @Override
    protected WordRecordInfo getInstance(Cursor cursor) {
        return new WordRecordInfo(cursor);
    }

    @Override
    public long insertItem(WordRecordInfo item) {
        long nWordId = -1;
        ArrayList<WordRecordInfo> arrWordRecords = getAll();
        // if WordTable has same item method returns id this item from table
        // else runs insert and add new item in WordTable
        for(WordRecordInfo wordRecord : arrWordRecords){
            if(wordRecord.getOriginalWord().equalsIgnoreCase(item.getOriginalWord())&&
                    wordRecord.getDescription().equalsIgnoreCase(item.getDescription())){
                nWordId = wordRecord.getId();
                break;
            }

        }
        if(nWordId==-1){
            nWordId = super.insertItem(item);
        }


        return nWordId;
    }

    public ArrayList<WordRecordInfo> getAllByUnitId(long nId){
        ArrayList<WordRecordInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadable();

        String query = "SELECT " +
                UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID +", " +
                UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID +", " +
                UnitsDBConstants.WordsTable.TABLE_FIELD_ID +", " +
                UnitsDBConstants.WordsTable.TABLE_FIELD_ORIGINAL_WORD +", " +
                UnitsDBConstants.WordsTable.TABLE_FIELD_DESCRIPTION_WORD +", " +
                UnitsDBConstants.WordsTable.TABLE_FIELD_UNIT_IS_WORD_LEARNED +" " +
                " FROM " + UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_NAME + " " +
                " JOIN " + UnitsDBConstants.WordsTable.TABLE_NAME + " "+
                " ON " + UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_WORD_ID + " " +
                " = " + UnitsDBConstants.WordsTable.TABLE_FIELD_ID +
                " WHERE " + UnitsDBConstants.LinkUnitsTableWordsTable.TABLE_FIELD_UNIT_ID + " = ?" ;

        String[] arrSelection = {Long.toString(nId)};
        Cursor cursor = db.rawQuery(query,arrSelection);


            if (cursor != null) {
                if (cursor.moveToFirst()) {

                    do {
                        arrResult.add(new WordRecordInfo(cursor));
                    } while (cursor.moveToNext());
                }
            } else
                Log.d("MyLog", "Cursor is null");


        cursor.close();
        return arrResult;
    }

    //method for setCount fields AllWords and IS_LEARNED in UnitRecordInfo
    public void addWordInfoToUnit(ArrayList<UnitRecordInfo> arrUnitRecordInfo){

        int nCount = 0;
        for(UnitRecordInfo unitRecordInfo : arrUnitRecordInfo){
            ArrayList<WordRecordInfo> arrWordList = getAllByUnitId(unitRecordInfo.getId());

            unitRecordInfo.setAllWords(arrWordList.size());
            for(WordRecordInfo wordRecordInfo : arrWordList){

                if(wordRecordInfo.isLearntWord()){
                    nCount++;
                }
            }
            unitRecordInfo.setLearntWords(nCount);
            nCount = 0;

        }

    }

    public int deleteItems(ArrayList<LinkRecordInfo> arrLinkRecords) {
        int nCount = 0;

        for(LinkRecordInfo recordInfo : arrLinkRecords){
            WordRecordInfo wordRecord = getItemById(recordInfo.getWordId());
            nCount += deleteItem(wordRecord);

        }

        return nCount;
    }

}
