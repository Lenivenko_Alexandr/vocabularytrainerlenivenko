package com.lenivenco.as.vocabularytrainer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.activities.InfoTestActivity;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;

import java.util.ArrayList;

public class ShowWordsListBaseAdapter extends BaseAdapter {

    private String m_strKey = "";
    private ArrayList<WordRecordInfo> m_arrayData;

    public ShowWordsListBaseAdapter(ArrayList<WordRecordInfo> arrayData){
        m_arrayData = arrayData;
    }

    public ShowWordsListBaseAdapter(ArrayList<WordRecordInfo> arrayData,String strName){
        m_arrayData = arrayData;
        m_strKey = strName;
    }
    @Override
    public int getCount() {
        return m_arrayData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrayData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((WordRecordInfo)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if(view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_list_of_words_by_one_unit, viewGroup, false);
        }

        WordRecordInfo wordRecordInfo = ((WordRecordInfo)getItem(position));

        TextView originalWordTextView = (TextView)view.findViewById(R.id.originalTextViewItemWordByOneUnit);
        originalWordTextView.setText(wordRecordInfo.getOriginalWord());

        TextView descriptionWordTextView = (TextView)view.findViewById(R.id.descriptionWordTextViewItemWordByOneUnit);
        descriptionWordTextView.setText(wordRecordInfo.getDescription());

        if(m_strKey.equals(InfoTestActivity.INFO_TEST)){
            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBarItemWordByOneUnit);
            ratingBar.setRating(wordRecordInfo.getNumberTrueAnswer());
            ratingBar.setVisibility(View.VISIBLE);
            /*CheckBox checkBox = (CheckBox) view.findViewById(R.id.isLearnCheckBoxItemWordByOneUnit);
            checkBox.setVisibility(View.VISIBLE);
            checkBox.setChecked(wordRecordInfo.isLearntWord());*/
        }


        return view;
    }
}
