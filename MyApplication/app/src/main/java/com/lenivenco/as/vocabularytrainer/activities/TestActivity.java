package com.lenivenco.as.vocabularytrainer.activities;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.adapters.TestPagerAdapter;
import com.lenivenco.as.vocabularytrainer.custom.ViewPagerWithoutScrolling;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TestActivity extends AppCompatActivity {

    private static final String KEY_ROUND = "KEY_ROUND";

    private static int INFO_TEST_COD = 101;
    private long m_unitId = -1;
    private ViewPagerWithoutScrolling m_viewPager = null;
    private WordEngine m_wordEngine = null;
    ArrayList<WordRecordInfoForTest> m_arrWordRecords = null;
    private int m_nRound = 0;
    private TestPagerAdapter m_testPagerAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        Intent intent  = getIntent();

        if(savedInstanceState!=null){
            m_nRound = savedInstanceState.getInt(KEY_ROUND);
        }

        if(!intent.equals(null)){
            Bundle bundle = intent.getExtras();
            if (!bundle.equals(null)){
                m_unitId = (long) bundle.getLong(ComposeActivity.KEY_ID,-1);

            }
        }
        m_wordEngine = new WordEngine(this);

        //if m_unitId equal -1 createArrForText return All WordRecordInfoForTest which exist in WordTable
        m_arrWordRecords = m_wordEngine.createArrayForTest(m_unitId);
        m_viewPager = (ViewPagerWithoutScrolling) findViewById(R.id.viewPagerTestActivity);
        m_testPagerAdapter = new TestPagerAdapter(getSupportFragmentManager(),m_arrWordRecords);
        setPagerAdapter(true);

    }

    public void setPagerAdapter(boolean isFistRound) {
        long seed = System.nanoTime();
        Collections.shuffle(m_arrWordRecords, new Random(seed));
        for(WordRecordInfo wordRecordInfo : m_arrWordRecords){
            Log.d("MLog",wordRecordInfo.getOriginalWord());
        }
        Log.d("MLog"," ");

        if(isFistRound){
            for(WordRecordInfoForTest infoForTest: m_arrWordRecords){
                infoForTest.setNullNumberTrueAnswer();
            }
        } else {
            m_testPagerAdapter.setArray(m_arrWordRecords);
            m_viewPager.getAdapter().notifyDataSetChanged();
        }

        m_viewPager.setAdapter(m_testPagerAdapter);


    }


    public void nextFragment() {
        if( m_viewPager.getCurrentItem() ==m_viewPager.getAdapter().getCount()-1){
            Intent intent = new Intent(this,InfoTestActivity.class);
            intent.putExtra(ComposeActivity.KEY_ID,m_unitId);
            intent.putExtra(InfoTestActivity.ROUND_INFO,++m_nRound);
            startActivityForResult(intent,TestActivity.INFO_TEST_COD);
        }else {
            m_viewPager.setCurrentItem(m_viewPager.getCurrentItem()+1);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==INFO_TEST_COD){
            //setPagerAdapter(false);
            if(resultCode==RESULT_CANCELED){
                finish();
                //TODO RESULT_CANCELED
            }else if(resultCode==RESULT_OK){
                setPagerAdapter(false);
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt(KEY_ROUND,m_nRound);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ROUND,m_nRound);
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.m_nRound = savedInstanceState.getInt(KEY_ROUND);
    }
}
