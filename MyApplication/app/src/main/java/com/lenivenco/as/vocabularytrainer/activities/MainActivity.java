package com.lenivenco.as.vocabularytrainer.activities;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.adapters.MainScreenPagerAdapter;
import com.lenivenco.as.vocabularytrainer.fragments.MainScreenFragment;


import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {


    private MainScreenPagerAdapter m_pagerAdapter = null;
    private ViewPager m_viewPager = null;
    private TabLayout m_tabLayout = null;
    private ArrayList<String> m_arrTitleForTabLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
    }

    private void findView() {
        m_arrTitleForTabLayout = new ArrayList<>();
        m_arrTitleForTabLayout.add(MainScreenFragment.EDIT_ACTIVITY_KEY);
        m_arrTitleForTabLayout.add(MainScreenFragment.LEARN_ACTIVITY_KEY);
        m_arrTitleForTabLayout.add(MainScreenFragment.TEST_ACTIVITY_KEY);

        m_viewPager = (ViewPager) findViewById(R.id.viewPagerMainActivity);
        m_tabLayout = (TabLayout) findViewById(R.id.layoutTabStripMainActivity);

        setPagerAdapter();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }




    public void setPagerAdapter() {
        m_pagerAdapter = new MainScreenPagerAdapter(this,getSupportFragmentManager(),m_arrTitleForTabLayout);
        m_viewPager.setAdapter(m_pagerAdapter);
        m_tabLayout.setupWithViewPager(m_viewPager);
        m_tabLayout.getTabAt(0).setIcon(R.drawable.selector_edit_image_button);
        m_tabLayout.getTabAt(1).setIcon(R.drawable.selector_learn_image_button);
        m_tabLayout.getTabAt(2).setIcon(R.drawable.selector_test_image_button);


        m_viewPager.setCurrentItem(1);


    }
}
