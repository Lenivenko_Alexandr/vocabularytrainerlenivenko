package com.lenivenco.as.vocabularytrainer.activities;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lenivenco.as.vocabularytrainer.R;
import com.lenivenco.as.vocabularytrainer.adapters.ShowWordsListBaseAdapter;
import com.lenivenco.as.vocabularytrainer.dialogs.EditUnitDialog;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.WordRecordInfoForTest;
import com.lenivenco.as.vocabularytrainer.model.engines.UnitEngine;
import com.lenivenco.as.vocabularytrainer.model.engines.WordEngine;

import java.util.ArrayList;

public class InfoTestActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String INFO_TEST = "INFO_TEST";
    public static final String KEY_CREATE_UNIT_WITH_MISTAKES = "CREATE_UNIT_WITH_MISTAKES";
    public static final String ROUND_INFO = "ROUND_INFO";
    private ListView m_infoListView = null;
    private ArrayList<WordRecordInfo> m_arrWordsData = null;
    private ShowWordsListBaseAdapter m_wordsAdapter = null;
    private long m_unitId = -1;
    private Button m_nextRound = null;
    private int m_nRound = 0;
    private WordEngine m_wordEngine = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_test);

        m_arrWordsData = new ArrayList<>();
        m_wordsAdapter = new ShowWordsListBaseAdapter(m_arrWordsData,INFO_TEST);
        createArray();

        TextView titleTextView = (TextView) findViewById(R.id.titleInfoTestActivity);
        titleTextView.setText(getResources().getString(R.string.round) + m_nRound);

        m_infoListView = (ListView) findViewById(R.id.infoListViewInfoActivity);
        m_infoListView.setAdapter(m_wordsAdapter);


        m_nextRound = (Button) findViewById(R.id.nextRoundButtonInfoActivity);
        m_nextRound.setOnClickListener(this);
        if(m_unitId==-1){
            titleTextView.setText("TEST RESULT");
            m_nextRound.setText("CREATE UNIT WITH MISTAKES");
            m_nextRound.setTag(KEY_CREATE_UNIT_WITH_MISTAKES);
        }
        if(m_nRound==3){
            m_nextRound.setText(getResources().getString(R.string.exit));
            m_nextRound.setTag(INFO_TEST);
        }
    }


    @Override
    public void onClick(View view) {
        if(view.getTag()!=null){
            if (view.getTag().toString().equals(INFO_TEST)){
                setResult(RESULT_CANCELED);
            }
            if (view.getTag().toString().equals(KEY_CREATE_UNIT_WITH_MISTAKES)){
                createNewMistakesUnit();
            }

        }else {
            //setResult(RESULT_OK);
        }
        //finish();

    }

    private void createNewMistakesUnit() {
        WordEngine wordEngine = new WordEngine(this);

        for(WordRecordInfo infoForTest : m_arrWordsData){
            if(infoForTest.getNumberTrueAnswer()>0){
                infoForTest.setLearntWord(true);
                wordEngine.updateItem(infoForTest);

            }

        }

        createDialog((long)-2);//if id==-2 it means in EditUnitDialog will create new Unit full words with mistakes);
        /*Intent intent = new Intent(this, EditUnitDialog.class);
        intent.putExtra(ComposeActivity.KEY_ID,(long)-2);//if id==-2 it means in EditUnitDialog will create new Unit full words with mistakes
        startActivity(intent);*/
    }

    public void createDialog(long nUnitId) {
        EditUnitDialog editUnitDialog = new EditUnitDialog(this,nUnitId);
        editUnitDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finish();
            }
        });
        editUnitDialog.setCancelable(false);
        editUnitDialog.setTitle(getResources().getString(R.string.setName));
        editUnitDialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();

        createArray();
    }

    private void createArray() {
        Intent intent  = getIntent();

        if(!intent.equals(null)){
            Bundle bundle = intent.getExtras();
            if (!bundle.equals(null)){
                m_unitId = (long) bundle.getLong(ComposeActivity.KEY_ID,-1);
                m_nRound = (int) bundle.getInt(ROUND_INFO);

            }
        }
        m_wordEngine = new WordEngine(this);
        m_arrWordsData.clear();
        if(m_unitId!=-1){
            m_arrWordsData.addAll(m_wordEngine.getAllByUnitId(m_unitId));
        }else {
            m_arrWordsData.addAll(m_wordEngine.getAll());
        }

        m_wordsAdapter.notifyDataSetChanged();
    }

}
