package com.lenivenco.as.vocabularytrainer.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

public class TranslateLoader extends AsyncTaskLoader<String> {

    private String mUrl;
    public TranslateLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public String loadInBackground() {

        if (mUrl == null) {
            return null;
        }

        // Perform the network request, parse the response, and extract a string translation word.
        String translationWord = QueryUtils.extractEarthquakes(mUrl);
        return translationWord;
    }
}
