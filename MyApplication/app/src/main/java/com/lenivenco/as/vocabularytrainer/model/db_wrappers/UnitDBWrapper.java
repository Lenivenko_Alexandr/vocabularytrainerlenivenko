package com.lenivenco.as.vocabularytrainer.model.db_wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.SyncStateContract;

import com.lenivenco.as.vocabularytrainer.constants.UnitsDBConstants;
import com.lenivenco.as.vocabularytrainer.model.LinkRecordInfo;
import com.lenivenco.as.vocabularytrainer.model.UnitRecordInfo;

public class UnitDBWrapper extends BaseDBWrapper<UnitRecordInfo>{

    public UnitDBWrapper(Context context) {
        super(context, UnitsDBConstants.UnitsTable.TABLE_NAME);
    }

    @Override
    protected String getIdFieldName() {
        return UnitsDBConstants.UnitsTable.TABLE_FIELD_ID;
    }

    @Override
    protected UnitRecordInfo getInstance(Cursor cursor) {
        return new UnitRecordInfo(cursor);
    }

    public boolean hasSameUnitName(String strUnitName){
        boolean bFlag = false;
        SQLiteDatabase database = getReadable();


        String strSelection = UnitsDBConstants.UnitsTable.TABLE_FIELD_UNIT_NAME + "=?";
        String[] arrSelectionArgs = {strUnitName};


        Cursor cursor = database.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    bFlag = true;
                }
            } finally {
                cursor.close();
            }
        }
        database.close();
        return bFlag;
    }

   /* @Override
    public int deleteItem(UnitRecordInfo item) {
        super.deleteItem(item);
        LinkRecordInfo recordInfo = new LinkRecordInfo(item.getId(),)
        return 0;
    }*/
}
